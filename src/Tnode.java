import java.util.*;

/** Tree with two pointers.
 * @since 1.8
 */
public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   private void setName(String name) {
      this.name = name;
   }

   private String getName(){
      return name;
   }

   private void setFirstChild(Tnode firstChild) {
      this.firstChild = firstChild;
   }

   private Tnode getFirstChild(){
      return firstChild;
   }

   private void setNextSibling(Tnode nextSibling) {
      this.nextSibling = nextSibling;
   }

   private Tnode getNextSibling(){
      return nextSibling;
   }

   Tnode (String s, Tnode child, Tnode sibling){
      setName(s);
      setFirstChild(child);
      setNextSibling(sibling);
   }

   Tnode(){
      this ("", null, null);
   }

   Tnode(String s){
      this (s, null, null);
   }

   Tnode(String s, Tnode child){
      this(s, child, null);
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      addNodeToStringRecursively(b, this);
      return b.toString();
   }

   private void addNodeToStringRecursively(StringBuffer buffer, Tnode currentNode){
      buffer.append(currentNode.getName());
      if(currentNode.getFirstChild() != null){
         buffer.append("(");
         addNodeToStringRecursively(buffer, currentNode.getFirstChild());
         buffer.append(",");
         addNodeToStringRecursively(buffer, currentNode.getFirstChild().getNextSibling());
         buffer.append(")");
      }
   }

   public static Tnode buildFromRPN (String pol) {
      try{
         checkRPNValidity(pol);
      } catch(Exception e) {
         throw new RuntimeException(e.getMessage());
      }
      LinkedList<Tnode> nodeStack = new LinkedList<>();
      StringTokenizer tokenizer = new StringTokenizer(pol);
      while(tokenizer.hasMoreTokens()){
         Tnode node = new Tnode(tokenizer.nextToken());
         if(isValidOp(node.getName())){
            switch (node.getName()) {
               case "SWAP": {
                  Tnode first = nodeStack.pop();
                  Tnode second = nodeStack.pop();
                  nodeStack.push(first);
                  nodeStack.push(second);
                  break;
               }
               case "DUP":
                  Tnode currentNode = nodeStack.pop();
                  nodeStack.push(currentNode);
                  nodeStack.push(copyTree(currentNode, new Tnode()));
                  break;
               case "ROT": {
                  Tnode first = nodeStack.pop();
                  Tnode second = nodeStack.pop();
                  Tnode third = nodeStack.pop();
                  nodeStack.push(second);
                  nodeStack.push(first);
                  nodeStack.push(third);
                  break;
               }
               default:
                  Tnode sibling = nodeStack.pop(); // Popping the sibling
                  Tnode olderSibling = nodeStack.pop();
                  olderSibling.setNextSibling(sibling);
                  node.setFirstChild(olderSibling);
                  nodeStack.push(node);
                  break;
            }
         } else {
            nodeStack.push(node);
         }
      }
      return nodeStack.pop();
   }

   private static Tnode copyTree(Tnode toCopy, Tnode newNode){
      newNode.setName(toCopy.name);
      if(toCopy.getNextSibling() != null){
         newNode.setNextSibling(copyTree(toCopy.getNextSibling(), new Tnode()));
      }
      if(toCopy.getFirstChild() != null){
         newNode.setFirstChild(copyTree(toCopy.getFirstChild(), new Tnode()));
      }
      return newNode;
   }

   public static void main (String[] param) {
      String rpn = "1 2 _";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
      // TODO!!! Your tests here
   }

   private static void checkRPNValidity (String pol){
      if(pol == null || pol.trim().length() == 0){
         throw new RuntimeException("Empty string");
      }
      int elementsOnStack = 0;
      StringTokenizer tokenizer = new StringTokenizer(pol);
      while(tokenizer.hasMoreTokens()){
         String token = tokenizer.nextToken();
         if(isValidOp(token)) {
            if(token.equals("DUP")){
               if(elementsOnStack < 1){
                  throw new RuntimeException(
                          "Not enough elements on the stack to perform DUP operation in expression " + pol);
               } else {
                  elementsOnStack += 1;
               }
            }
            else if(token.equals("ROT")){
               if(elementsOnStack < 3){
                  throw new RuntimeException(
                          "Not enough elements on the stack to perform ROT operation in expression " + pol);
               }
            }
            else if(elementsOnStack < 2){
               throw new RuntimeException("Not enough elements on the stack to perform " +
                       token + " operation in expression " + pol);
            } else if(!token.equals("SWAP")) {
               elementsOnStack -= 1;
            }
         } else {
            try {
               Double.parseDouble(token);
               elementsOnStack += 1;
            } catch (Exception exception){
               throw new RuntimeException(token + " is not a valid number in expression " + pol + "\n");
            }
         }
      }
      if(elementsOnStack > 1){
         throw new RuntimeException("All elements on the stack have not been consumed in expression " + pol);
      }
   }

   private static boolean isValidOp(String s){
      return (s.equals("+") ||
              s.equals("-") ||
              s.equals("*") ||
              s.equals("/") ||
              s.equals("ROT") ||
              s.equals("DUP") ||
              s.equals("SWAP"));
   }
}

